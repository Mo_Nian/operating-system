#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<windows.h>
//#define NULL 0
int shumu=0;
//进程的内容结构体
struct node {
	char a[20];
    //int a;
   // char ch;
};
typedef struct node Node;
 
//进程PCB结构体
struct jincheng{
	int pid;
    int youxian;
    float luntime;
    float zhantime;
    char zhuangtai;    //a表示执行，b表示动态就绪
    Node *neirong;
    struct jincheng *next;
};
typedef struct jincheng Jincheng;
 
Jincheng *jiuxu,*yunxing,*p,*q;
 
//换出进程函数
void huanchu(int a){
    p=jiuxu;
    while(p->pid!=a&&p!=NULL){
		q=p;
        p=p->next;
	}
	
    if(p==NULL){
        printf("该进程不在内存里!\n");
        return;
	}
    if(p==jiuxu){
		q=yunxing->next;
        yunxing->next=jiuxu;
        q->next=jiuxu->next;
		jiuxu=q;
	}else{
        q->next=yunxing->next;
		q->next->next=p->next;
        yunxing->next=p;
	}
 }
 
//杀死正在运行进程函数
void shasi(){
	yunxing->next=NULL;
    printf("运行的进程已经杀死!\n");
    return;
}
 
 
//创建新进程后与正在运行进程比较优先级并根据优先级判断谁该占用处理机
int bijiao(){
    int i,j;
    p=jiuxu;
    while(p->next!=NULL){
        p=p->next;
	}
	q=p;                 //q指向进程的末尾，即新建的进程
    i=q->youxian;        //i代表新建进程的优先级
    j=yunxing->next->youxian;  //j代表正在执行进程的优先级
    if(i>j){                   //如果新建的进程的优先级高于正在执行程序的优先级
        p=jiuxu;
        if(p==q){         //就绪队列的进程中只有一个进程。也就是那个新建的进程
			jiuxu->next=yunxing->next;
			yunxing->next=jiuxu;
			jiuxu=jiuxu->next;
            return 1;
		}else{
            while(p->next!=q){
                p=p->next;
			}                    //执行完后 p 指针在 q指针前面
		    
            p->next=yunxing->next;   //将正在执行的进程放置p的后面
            yunxing->next=q;      //将q放置在正在执行列表中，把处理机交给优先级高级的进程
			p->next->next=q->next;
            yunxing->next->next=NULL;
			
            return 1;
		}
	}
  
    else
		return -1;
}
 
 
//创建新的进程函数
int create(){
    int i= 0;
    if(shumu>5){
        printf("内存已满请先换出进程!\n");
        i = -1;
        return i;
	}else{
        if(jiuxu==NULL){  //如果就绪队列中没有进程的话
            p=(Jincheng*)malloc(sizeof(Jincheng));
            printf("请输入新进程的名字（数字）:\n");
            scanf("%d",&p->pid);
            printf("请输入新进程的优先级:（数字）\n");
            scanf("%d",&p->youxian);
            p->luntime=3.5;
            p->zhantime=3;
			printf("请输入进程内容：\n");
            p->neirong=(Node*)malloc(sizeof(Node));
			scanf("%s",p->neirong->a);
           // p->neirong=NULL;
            p->zhuangtai='b'; //新建进程的状态设置为“就绪”
            p->next=NULL;
            jiuxu=p;
            shumu++;
            i=1;
		}else{//如果就绪队列不是空队列
            p=jiuxu;
            while(p->next!=NULL){
                p=p->next;         //p一直指向就绪队列的队尾
			}
            q=(Jincheng*)malloc(sizeof(Jincheng));
            q->next=NULL;
            p->next=q;             //在就绪队列的队尾加入新建的进程
            printf("请输入新进程的名字（数字）:\n");
            scanf("%d",&q->pid);
            printf("请输入新进程的优先级:（数字）\n");
            scanf("%d",&q->youxian);
            q->luntime=3.5;
            q->zhantime=3;
			printf("请输入进程内容：\n");
            q->neirong=(Node*)malloc(sizeof(Node));
	        scanf("%s",q->neirong->a);
            //q->neirong=NULL;
            q->zhuangtai='b';     //新建进程的状态设置为就绪
            shumu++;
			i=1;
		}
	}
    return i;
}
 
 
//从活动就绪进程队列中找到一个优先级最高的进程并为它分配处理机
int zhixing(){
    int i,j;
    p=jiuxu;
    if(yunxing->next!=NULL){
        return -1;
	}
    i=jiuxu->youxian;
    p=jiuxu->next;
    while(p!=NULL){
        j=p->youxian;
        if(i>=j){
            p=p->next;
		}
        if(i<j){
            i=p->youxian;
            p=p->next;
		}
	}
    if(jiuxu->youxian==i){
        yunxing->next=jiuxu;
        jiuxu=jiuxu->next;
        yunxing->next->next=NULL;
	}else{
        p=jiuxu;
        while(i!=p->youxian){
            q=p;
            p=p->next;
		}                          // q是p前面的指针
        p->zhuangtai='a';
          
        yunxing->next=p;           //将p放入执行列表
        q->next=p->next;          //将优先级高的节点舍去
		yunxing->next->next=NULL;
	}
    return 1;
}

// 查看当前进程信息
void chakan(){
    p=yunxing->next;
    printf("该执行进程的名字为:%d\n",p->pid);
    printf("该执行进程的的优先级:%d\n",p->youxian);
    printf("该执行进程的轮转时间为:%f\n",p->luntime);
    printf("该执行进程占用cpu的时间为:%f\n",p->zhantime);
    printf("该执行的进程内容为:\n");
    printf("%s",p->neirong->a);
    //printf("%c",p->neirong->ch);
    printf("\n");
}
 
//进程间的通信
void tongxing(int a){
	char c[20];
	int i;
    q=jiuxu;
    while((q != NULL) && (q->pid != a)){
        q=q->next;
	}//q为id为a的进程
    if(q == NULL){
        printf("所输入的进程不在内存中!\n");
        return;
	}
    p=yunxing->next;//p为正在执行的进程
    
	for(i = 0; i < 20; i++){
		c[i] = p->neirong->a[i];
        p->neirong->a[i] = q->neirong->a[i];
		q->neirong->a[i] = c[i];
	}
	//q->neirong=(Node*)malloc(sizeof(Node));
    //q->neirong->a=p->neirong->a;
    //q->neirong->ch=p->neirong->ch;//将正在执行的进程内容复制给id为a的进程
    printf("通信成功!\n");
    return;
}
 
void init(){
	yunxing=(Jincheng*)malloc(sizeof(Jincheng));
    yunxing->next=NULL;
    jiuxu=(Jincheng*)malloc(sizeof(Jincheng));
    jiuxu=NULL;
}
 
int main(){
	int i,n=1;
    int k,j,s;
	
    init();
 
    printf("——————————JinChengGuanLi——————————\n");
       
    while(n==1){
        printf("********************************************\n");
        printf("*               进程演示系统               *\n");
        printf("********************************************\n");
        printf("     1.创建新的进程      2.查看运行进程 \n");
        printf("     3.换出某个进程      4.杀死运行进程 \n");
        printf("     5.进程之间通信      6.退出系统 \n");
        printf("********************************************\n");
        printf("请选择（1～6）\n");
        scanf("%d",&i);
        switch(i){
        case 1:k=create();
              if(k==1){
				  printf("进程创建成功!\n");
			  }
              if(yunxing->next == NULL && jiuxu != NULL){
                   printf("由于只有一个进程所以为它分配处理机.\n");
                   yunxing->next=jiuxu;
                   jiuxu=NULL;
 
				   //system("CLS");
                   continue;
			  }
              k=bijiao();
  
              if(k==1){
                  printf("由于新进程的优先级高于正在执行的进程所以正在执行的\n");
                  printf("进程让出处理机交给新进程,而它变为活动就绪!\n");
			  }
              if(k!=1)
                  printf("新进程的优先级低于正在运行的进程所以它只有等待\n");
			  //system("CLS");
              break;
         case 2:
              if(yunxing->next==NULL){
                  printf("没有进程处于执行状态!\n");
                  continue;
			  }
              chakan();break;
         case 3:
              if(jiuxu==NULL){
                  printf("内存中已经没有处于活动就绪的进程了请创建！\n");
                  continue;
			  }
              printf("已有处于活动就绪进程的名字为:\n");
              p=jiuxu;
              printf("(");
              while(p!=NULL){
                  printf("%d ",p->pid);
                  p=p->next;
			  }
              printf(")\n");
              printf("请输入要换出的处于活动就绪进程的名字\n");
              scanf("%d",&s);
              huanchu(s);
              //if(jiuxu==NULL)
             //     printf("内存中已经没有活动就绪进程!\n");
             // else
			 // {
              //    printf("已有处于活动就绪进程的名字为:\n");
              //    p=jiuxu;
              //    printf("(");
               //   while(p!=NULL)
			//	  {
               //       printf("%d ",p->pid);
               //       p=p->next;
  
			//	  }
              //    printf(")\n");
			 // }
              break;
    case 4:
         if(yunxing->next==NULL){
             printf("没有处于执行状态的进程!\n");
             continue;
		 }
         shasi();
         if(jiuxu==NULL){
             printf("已经没有处于活动就绪的进程请创建!\n");
             continue;
		 }
         j=zhixing();
         if(j==1){
              printf("已为一个动态就绪进程中优先级最高的进程分配处理器!\n");
		 }
         break;
     case 5:
          if(jiuxu==NULL){
              printf("内存中已经没有处于活动就绪的进程了请创建!\n");
              continue;
		  }
          if(yunxing->next==NULL){
              printf("没有处于执行状态的进程!\n");
              continue;
		  }
          printf("请输入要与正在运行的进程进行进程通讯的进程名字\n");
          scanf("%d",&s);
          tongxing(s);
          break;
      case 6:exit(0);
      default:n=0;
		}
    }
	return 0;
}
